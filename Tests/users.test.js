const request = require('supertest');
const ApiUrl = "http://localhost:3000";



describe('GET /produtos', () => { // test suite

it('should return 200', () => { // test
  return request(ApiUrl)
  .get('/produtos')
  .set('Accept', 'application/json')
  .expect(200);
});

it('should be fail', () => { // test
  return request(ApiUrl)
  .get('/produtos')
  .set('Accept', 'application/json')
  .expect(201);
});

});
